#include "lc3.h"

/////// For you to implement
//#define _DEBUG
#ifdef _DEBUG
#include <stdio.h>
# define lc3_debug(x...) \
do { \
	printf("DEBUG:%10s:%10s:% 4d: ", __FILE__, __func__, __LINE__); \
	printf(x); \
	printf("\n"); \
} while(0)
#else
# define lc3_debug(x...) ((void) 0)
#endif

/* The provided sign-extend doesnt work */
#define _SXT(w, l) \
	__SXT((w), (l))
#define __SXT(w, l) \
	(w | (((w >> (l - 1)) & 1) ? (-1 << l) : 0))

#define _EXTRACT_BITS(w, l, s)	\
	(((w) >> (s)) & ((1 << (l)) - 1))

#define EXTRACT_PCOFFSET9(i)	\
	_SXT(_EXTRACT_BITS(i, 9, 0), 9)

#define EXTRACT_STx_SR(i) _EXTRACT_BITS(i, 3, 9)
#define EXTRACT_DR(i) _EXTRACT_BITS(i, 3, 9)

#define EXTRACT_IS_IMM(i) _EXTRACT_BITS(i, 1, 5)

#define EXTRACT_IMM(i) \
	_SXT(_EXTRACT_BITS(i, 5, 0), 5)

#define EXTRACT_SR1(i) \
	_EXTRACT_BITS(i, 3, 6)

#define EXTRACT_SR2(i) \
	_EXTRACT_BITS(i, 3, 0)

#define EXTRACT_OPCODE(i) \
	_EXTRACT_BITS(i, 4, 12)

#define EXTRACT_BR_NZP(i)	\
	_EXTRACT_BITS(i, 3, 9)

/* Zero all signals so the ones we don't set in Decode won't interfere */
static void zero_signals_de(struct Signals &signals) {
	signals.de.operand1 = 0;
	signals.de.operand2 = 0;
	signals.de.dest_reg  = 0;
	signals.de.arith_type = ARITH_NONE;
	signals.de.mem_op = MEM_NONE;
	signals.de.mem_data = 0;
	signals.de.reg_write = REG_WRITE_NONE;
	signals.de.set_flags = FLAGS_PREV;
	signals.de.br_flags = 0;
	signals.de.old_flags = 0;
}

/* Create flags bitmap from value */
static inline unsigned short get_flags(unsigned short val) {
	unsigned short result = 0;
	if (((short) val) < 0)
		result|= N_FLAG;
	else if (val == 0)
		result |= Z_FLAG;
	else
		result |= P_FLAG;
	return result;
}


void LC3::Fetch(unsigned short &ins, struct Signals &signals)
{
	lc3_debug("+++ fetch begin +++");

	ins = this->ReadMem(this->pc);
	this->pc += 2;
	signals.fe.next_pc = this->pc;
	lc3_debug("current %04x, next %04x", this->pc - 2, this->pc);

	lc3_debug("--- fetch end ---");
}

void LC3::Decode(unsigned short ins, struct Signals &signals)
{
	lc3_debug("+++ decode begin +++");

	lc3_debug("instruction %04x", ins);
	zero_signals_de(signals);
	switch (EXTRACT_OPCODE(ins)) {
	case ADD_OPCODE:
	case AND_OPCODE:
		lc3_debug("%s", EXTRACT_OPCODE(ins) == ADD_OPCODE ?
				"ADD" : "AND");
		signals.de.operand1 = this->regs[EXTRACT_SR1(ins)];
		lc3_debug("op1 = %04x", signals.de.operand1);
		signals.de.operand2 = (EXTRACT_IS_IMM(ins)) ?
					EXTRACT_IMM(ins) :
					this->regs[EXTRACT_SR2(ins)];
		lc3_debug("op2 = %04x %c", signals.de.operand2,
				EXTRACT_IS_IMM(ins) ? 'I' : ' ');
		signals.de.mem_op = MEM_NONE;
		signals.de.dest_reg = EXTRACT_DR(ins);
		lc3_debug("dest_reg = %01x", signals.de.dest_reg);
		signals.de.reg_write = REG_WRITE_ARITH;
		signals.de.set_flags = FLAGS_EXEC;

		if (EXTRACT_OPCODE(ins) == ADD_OPCODE) {
			signals.de.arith_type = ARITH_ADD;
		} else if (EXTRACT_OPCODE(ins) == AND_OPCODE) {
			signals.de.arith_type = ARITH_AND;
		} else {
			signals.de.arith_type = ARITH_NONE;
		}
		break;

	case ST_OPCODE:
	case LD_OPCODE:
		lc3_debug("%s", EXTRACT_OPCODE(ins) == ST_OPCODE ?
				"ST" : "LD");

		signals.de.operand1 = signals.fe.next_pc;
		signals.de.operand2 = EXTRACT_PCOFFSET9(ins);
		signals.de.arith_type = ARITH_ADD;
		if (EXTRACT_OPCODE(ins) == ST_OPCODE) {
			signals.de.mem_data = this->regs[EXTRACT_STx_SR(ins)];
			signals.de.mem_op = MEM_WRITE;
			lc3_debug("reg%d = %04x", EXTRACT_STx_SR(ins),
					signals.de.mem_data);
		} else if (EXTRACT_OPCODE(ins) == LD_OPCODE) {
			signals.de.mem_op = MEM_READ;
			signals.de.set_flags = FLAGS_MEM;
			signals.de.reg_write = REG_WRITE_MEM;
			signals.de.dest_reg = EXTRACT_DR(ins);
			lc3_debug("dest_reg = reg%d", EXTRACT_DR(ins));
		}
		lc3_debug("PCoffset9 = %04x", signals.de.operand2);
		break;

	case BR_OPCODE:
		lc3_debug("BR%c%c%c", (EXTRACT_BR_NZP(ins) & P_FLAG) ? 'p' : ' ',
				(EXTRACT_BR_NZP(ins) & Z_FLAG) ? 'z' : ' ',
				(EXTRACT_BR_NZP(ins) & N_FLAG) ? 'n' : ' ');

		signals.de.operand1 = signals.fe.next_pc;
		signals.de.operand2 = EXTRACT_PCOFFSET9(ins);
		lc3_debug("op2 = %04d", signals.de.operand2);
		signals.de.arith_type = ARITH_ADD;
		signals.de.br_flags = EXTRACT_BR_NZP(ins);
		signals.de.old_flags = this->flags;
		break;

	default:
		lc3_debug("Executing unknown opcode");
	}

	lc3_debug("--- decode end ---");
}

void LC3::Exec(struct Signals &signals)
{
	lc3_debug("+++ exec begin +++");

	switch (signals.de.arith_type) {
	case ARITH_ADD:
		signals.ex.arith_res = signals.de.operand1 +
					signals.de.operand2;
		lc3_debug("arith_res = %04x + %04x = %04x", signals.de.operand1,
				signals.de.operand2, signals.ex.arith_res);
		break;
	case ARITH_AND:
		signals.ex.arith_res = signals.de.operand1 &
					signals.de.operand2;
		lc3_debug("arith_res = %04x & %04x = %04x", signals.de.operand1,
				signals.de.operand2, signals.ex.arith_res);
		break;
	}

	/* Flags of arith result */
	signals.ex.new_flags = get_flags(signals.ex.arith_res);

	lc3_debug("--- exec end ---");

}

void LC3::WbMem(struct Signals &signals)
{
	unsigned short mem_out;
	unsigned short mem_flags;

	lc3_debug("+++ wb/mem begin +++");

	/* Access memory */
	switch(signals.de.mem_op) {
	case MEM_READ:
		mem_out = this->ReadMem(signals.ex.arith_res);
		mem_flags = get_flags(this->regs[signals.de.dest_reg]);
		break;

	case MEM_WRITE:
		this->WriteMem(signals.ex.arith_res, signals.de.mem_data);
		break;
	}

	/* Write to registers */
	switch (signals.de.reg_write) {
	case REG_WRITE_ARITH:
		this->regs[signals.de.dest_reg] = signals.ex.arith_res;
		lc3_debug("write arith_res to reg%d: %04x",
				signals.de.dest_reg,
				signals.ex.arith_res);
		break;
	case REG_WRITE_MEM:
		this->regs[signals.de.dest_reg] = mem_out;
	}

	/* Update PC if we're supposed to branch */
	if (signals.de.br_flags == (P_FLAG | Z_FLAG | N_FLAG) ||
			signals.de.br_flags & signals.de.old_flags) {
		lc3_debug("branch to %04x", signals.ex.arith_res);
		this->pc = signals.ex.arith_res;
	}

	/* Update flags if needed */
	switch (signals.de.set_flags) {
	case FLAGS_EXEC:
		this->flags = signals.ex.new_flags;
		break;
	case FLAGS_MEM:
		this->flags = mem_flags;
		break;
	}

	if (signals.de.set_flags != FLAGS_PREV) {
		lc3_debug("setting flags: %c%c%c",
				(this->flags & P_FLAG) ? 'p' : ' ',
				(this->flags & Z_FLAG) ? 'z' : ' ',
				(this->flags & N_FLAG) ? 'n' : ' ');
	}

	lc3_debug("--- wm/mem end ---");
}

