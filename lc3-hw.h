/*
 * LC3-hw.h
 */

#ifndef LC3_HW_H_
#define LC3_HW_H_

typedef enum {
	ARITH_NONE,
	ARITH_AND,
	ARITH_ADD
} arith_type_t;

typedef enum {
	MEM_NONE,
	MEM_READ,
	MEM_WRITE
} mem_op_t;


typedef enum {
	FLAGS_PREV,
	FLAGS_EXEC,
	FLAGS_MEM
} set_flags_t;

typedef enum {
	REG_WRITE_NONE,
	REG_WRITE_ARITH,
	REG_WRITE_MEM
} reg_write_t;

struct Signals
{
	// Fetch:
	struct {
		unsigned short next_pc;
	} fe;

	// Decode:
	struct {
		unsigned short operand1;
		unsigned short operand2;
		unsigned short dest_reg;
		arith_type_t arith_type;
		mem_op_t  mem_op;
		unsigned short mem_data;
		reg_write_t reg_write;
		set_flags_t  set_flags;
		unsigned short br_flags;
		unsigned short old_flags;
	} de;

	// Execute
	struct {
		unsigned short arith_res;
		unsigned short new_flags;
	} ex;

	// MemWriteBack: no signals, last stage
};

#endif
