#include <iostream>
#include "lc3.h"
#include "compiler.h"

using namespace std;

const struct InsFieldBits insFieldBits[] = {
	{IMM5, 0, 4, true},
	{IMM, 5, 5, false},
	{OFFSET6, 0, 5, true},
	{PCOFFSET9, 0, 8, true},
	{TRAPVECT8, 0, 7, false},
	{BASER, 6, 8, false},
	{DR, 9, 11, false},
	{SR, 9, 11, false},
	{SR1, 6, 8, false},
	{SR2, 0, 2, false},
	{OPCODE, 12, 15, false},
	{NZP, 9, 11, false},
	{NO_FIELD, 0, 0, false}
};

// Write to memory a value
void LC3::WriteMem(unsigned short addr, unsigned short val)
{
	*(unsigned short*)(mem+addr) = val;
}

// Read from memory a value
unsigned short LC3::ReadMem(unsigned short addr)
{
	return *(unsigned short*)(mem+addr);
}

LC3::LC3() : flags(0), pc(0)
{
	for (int i=0; i < REGS_NUM; i++)
		regs[i] = 0;
	memset(mem, 0, MEM_SIZE);
}

void LC3::DumpMem()
{
	for (int i=0; i<MEM_SIZE; i++)
		cout << hex << (int)mem[i] << endl;
}

void LC3::Run(int steps)
{
	for (int i=0; i<steps; i++) {
		unsigned short opcode;
		struct Signals signals;
		memset(&signals, 0, sizeof(signals));
		Fetch(opcode, signals);
		Decode(opcode, signals);
		Exec(signals);
		WbMem(signals);
	}
}

int main(int argc, char *argv[]) {
	if (argc < 3) {
		cout << "usage: lc3 [asm filename] [cycles]" << endl;
		return -1;
	}

	LC3 *lc3 = new LC3();
	Compiler* compiler = new Compiler(argv[1], lc3);

	int cycles = atoi(argv[2]);
	if (cycles <= 0) {
		cout << "number of cycles must be great than zero" << endl;
		return -1;
	}
	
	compiler->Compile();
	lc3->Run(cycles);
	lc3->DumpMem();
	delete compiler;
	delete lc3;
	return 0;
}

